package smlk.animator.tutorial.ridesharingservice.configurations.simpleconfiguration

import org.junit.jupiter.api.Test
import org.scenariotools.smlk.runTest
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration

class SimpleConfigurationTests {

    @Test
    fun testInteractionSystemConfiguration() = simpleRSSConfiguration().second.test()

}