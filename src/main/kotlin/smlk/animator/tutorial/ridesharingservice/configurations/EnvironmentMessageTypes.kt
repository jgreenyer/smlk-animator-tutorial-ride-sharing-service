package smlk.animator.tutorial.ridesharingservice.configurations

import smlk.animator.tutorial.ridesharingservice.model.system.CustomerBackEnd

val environmentMessageTypes = listOf(
    CustomerBackEnd::requestRide
)