package smlk.animator.tutorial.ridesharingservice.configurations.simpleconfiguration

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.sends
import org.scenariotools.smlk.symbolicEvent
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.label
import smlk.animator.tutorial.ridesharingservice.configurations.environmentMessageTypes
import smlk.animator.tutorial.ridesharingservice.model.`inter-component`.allInterComponentScenarios
import smlk.animator.tutorial.ridesharingservice.model.data.Location
import smlk.animator.tutorial.ridesharingservice.model.environment.CustomerFrontEnd
import smlk.animator.tutorial.ridesharingservice.model.specification.allSpecificationScenarios
import smlk.animator.tutorial.ridesharingservice.model.system.CustomerBackEnd
import smlk.animator.tutorial.ridesharingservice.model.system.Routing


// services
val cfe = CustomerFrontEnd("CFE", 100.0, 100.0)
val cbe = CustomerBackEnd("CBE", 300.0, 300.0)
val routing = Routing("Routing", 300.0, 500.0)

// data
val locFHDW = Location("FHDW")
val locAirport = Location("Airport")

val instances = listOf<Instance>(
    cfe,
    cbe,
    routing
)


fun simpleRSSConfiguration() : Pair<String, InteractionSystemConfiguration>{

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = instances,
        testScenario = scenario {
            label("Starting Simple RSS Test")
            request(cfe sends cbe.requestRide(locFHDW, locAirport))
            //request(cfe sends cbe.requestRide(locAirport, locFHDW))
        },
        guaranteeScenarios = setOf(
            allSpecificationScenarios,
            allInterComponentScenarios
            //,setOf(shadowScenario) // create a red shadow around components that received messages
        ).flatten().toSet()
    )

    return "Simple RSS Configuration" to configuration
}
