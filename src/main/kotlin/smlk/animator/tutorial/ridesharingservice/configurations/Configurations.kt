package smlk.animator.tutorial.ridesharingservice.configurations

import org.scenriotools.smlk.animator.model.InteractionSystemConfigurations
import smlk.animator.tutorial.ridesharingservice.configurations.simpleconfiguration.simpleRSSConfiguration

val configurations = InteractionSystemConfigurations(
        mapOf(
                simpleRSSConfiguration()
        )
)
