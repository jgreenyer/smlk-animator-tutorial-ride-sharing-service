package smlk.animator.tutorial.ridesharingservice.model.data

data class Passenger(val id:String, val name:String)