package smlk.animator.tutorial.ridesharingservice.model.`inter-component`

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.sends
import org.scenariotools.smlk.symbolicEvent
import smlk.animator.tutorial.ridesharingservice.configurations.simpleconfiguration.routing
import smlk.animator.tutorial.ridesharingservice.model.data.Location
import smlk.animator.tutorial.ridesharingservice.model.environment.CustomerFrontEnd
import smlk.animator.tutorial.ridesharingservice.model.system.CustomerBackEnd

val customerRequestsRideForwardedToRouting = scenario(CustomerBackEnd::requestRide.symbolicEvent()) {
    val cfe = it.sender as CustomerFrontEnd
    val cbe = it.receiver
    //val customerID = it.parameters[0] as Location
    val fromLoc = it.parameters[0] as Location
    val toLoc = it.parameters[1] as Location

    var expectedDelay = 0


    scenario{
        // Interact with Routing..
        request(cbe sends routing.requestRide(fromLoc, toLoc))
        //
        expectedDelay = 20
    }.before(CustomerFrontEnd::rideOrderRespomse.symbolicEvent()) // WARNING: Too restrictive, we need to discuss what to do here.



    //response to cfe
    requestParamValuesExact(cbe sends cfe.rideOrderRespomse(expectedDelay))


}