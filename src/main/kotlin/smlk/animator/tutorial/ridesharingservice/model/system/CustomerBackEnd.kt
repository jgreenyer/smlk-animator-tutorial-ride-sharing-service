package smlk.animator.tutorial.ridesharingservice.model.system

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import smlk.animator.tutorial.ridesharingservice.model.data.Location
import tornadofx.c

class CustomerBackEnd (name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")) {

    fun requestRide(fromLoc : Location, toLoc: Location) = event(fromLoc, toLoc){}
}