package smlk.animator.tutorial.ridesharingservice.model.environment

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

class CustomerFrontEnd(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")){
    fun rideOrderRespomse(estimatedDelay: Int) = event(estimatedDelay){ }

}
