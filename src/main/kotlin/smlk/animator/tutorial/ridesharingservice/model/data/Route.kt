package smlk.animator.tutorial.ridesharingservice.model.data

data class Route(val id:String, var stops:List<Stop>)

data class Stop(val location:Location, val pickUp:Set<Passenger>, val dropOff:Set<Passenger>)