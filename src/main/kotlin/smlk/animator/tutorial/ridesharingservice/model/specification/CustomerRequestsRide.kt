package smlk.animator.tutorial.ridesharingservice.model.specification

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.sends
import org.scenariotools.smlk.symbolicEvent
import org.scenriotools.smlk.animator.smlkextension.instancelabel
import smlk.animator.tutorial.ridesharingservice.model.data.Location
import smlk.animator.tutorial.ridesharingservice.model.environment.CustomerFrontEnd
import smlk.animator.tutorial.ridesharingservice.model.system.CustomerBackEnd


//val sc1 = scenario(<trigger event>){
//    <scenario body>
//}

val customerRequestsRideScenario = scenario(CustomerBackEnd::requestRide.symbolicEvent()) {
    val cfe = it.sender as CustomerFrontEnd
    val cbe = it.receiver
    //val customerID = it.parameters[0] as Location
    val fromLoc = it.parameters[0] as Location
    val toLoc = it.parameters[1] as Location
    //instancelabel(cbe,"Received request to go from $fromLoc to $toLoc")
    requestParamValuesMightVary(cbe sends cfe.rideOrderRespomse(10))
}
